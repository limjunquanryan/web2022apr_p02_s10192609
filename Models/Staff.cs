﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10192609.Models
{
    public class Staff
    {
        [Display(Name = "ID")] public int StaffId { get; set; }
        [Required] [StringLength(50, ErrorMessage = "Please keep Name length under 50 characters!")] public string Name { get; set; }
        public char Gender { get; set; }
        [Display(Name = "Date of Birth")] [DataType(DataType.Date)] public DateTime? DOB { get; set; }
        public string Nationality { get; set; }
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists] [Display(Name = "Email Address")] [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Please enter a valid Email Address")] public string Email { get; set; }
        [Display(Name = "Monthly Salary (SGD)")] [DisplayFormat(DataFormatString = "{0:#,##0.00}")] [Range(1.00, 10000.00, ErrorMessage = "Please enter a Salary between 1.00 and 10000.00!")] public decimal Salary { get; set; }
        [Display(Name = "Full-Time Staff")] public bool IsFullTime { get; set; }
        [Display(Name = "Branch")] public int? BranchNo { get; set; }
    }
}
